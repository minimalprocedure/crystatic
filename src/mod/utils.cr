# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

require "colorize"

module Utils
  extend self

  SUFFIX_ERROR = "ERR".colorize(:red)
  SUFFIX_MESSAGE = "MSG".colorize(:blue)

  def error(message, exit_code = nil)
    STDERR.puts("#{SUFFIX_ERROR}: #{message}")
    STDERR.flush
    exit(exit_code < 1 ? 1 : exit_code) if exit_code
  end

  def message(message)
    STDOUT.puts("#{SUFFIX_MESSAGE}: #{message}")
    STDOUT.flush
  end

  def message(suffix, message)
    STDOUT.puts("#{SUFFIX_MESSAGE}: #{suffix.colorize(:green)}: #{message}")
    STDOUT.flush
  end

  def prepare_buildroot(root : String, cryfile : String)
    buildroot = File.join(root, "_build")
    FileUtils.mkdir_p(buildroot)
    File.join(buildroot, File.basename(cryfile, File.extname(cryfile)))
  end

  def execute(cmd_line, chdir = nil)
    out_channel = IO::Memory.new
    error_channel = IO::Memory.new
    status = Process.run(
      cmd_line, shell: true, output: out_channel, error: error_channel,
      chdir: chdir)
    error_string = error_channel.to_s
    out_string = out_channel.to_s
    error_channel.close
    out_channel.close
    {status, out_string.strip, error_string.strip}
  end
end
