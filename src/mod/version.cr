module Version
  extend self
  MAJOR="0"
  MINOR="6"

  macro finished
    BUILD = Time.local.to_s("%Y%m%d%H%M%S")

    def get
      "#{Version::MAJOR}.#{Version::MINOR}.#{Version::BUILD}"
    end
  end

end
