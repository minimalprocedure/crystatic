# TODO: Write documentation for `Crystatic`

require "./driver"

module Crystatic
  extend self

  include Driver

  def compile(
    cryfile = nil,
    exefile = nil,
    projroot = nil,
    libsroot = nil,
    cc = "gcc",
    ld = nil,
    target = "x86_64-pc-linux-gnu",
    cryflags = [] of String,
    linkflags = [] of String,
    verbose = false
  )
    cc_line_parts = Driver.execute_crystal(
      projroot, cryfile,
      target: target,
      cryflags: cryflags,
      linkflags: linkflags,
      verbose: verbose)

    if cc_line_parts

      objcode = get_objcode(cc_line_parts)
      linkflags = get_flags_flags(cc_line_parts)
      static_libs = search_static_libs(libsroot,
        get_libs_values(cc_line_parts))
      static_libs = static_libs_found?(static_libs)
      libpaths, libs = linker_libs(static_libs)

      execute_linker(
        objcode, exefile,
        cc: cc,
        ld: ld,
        linkflags: linkflags,
        libpaths: libpaths,
        libs: libs,
        verbose: verbose)
    end
  end
end
