# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

require "file_utils"
require "./utils"

module Driver
  extend self

  alias TPart = NamedTuple(flags: Array(String), values: Array(String))

  NOT_FOUND      = "NOT_FOUND"
  NOT_FOUND_CODE = 1000
  NULL_LIB_NAME  = ""

  CC_STATIC  = "-static"
  CRY_STATIC = "-#{CC_STATIC}"

  STDLIBS = [
    {NULL_LIB_NAME, "libgcc.a"},
    {NULL_LIB_NAME, "libgomp.a"},
    {NULL_LIB_NAME, "libc.a"},
  ]

  MATCH_CC_LINE_PARTS = [
    "(?(?=cc)",
    "(?>cc\\s+(?<objcode>\\S*))",
    "|(?(?=-o)",
    "(?>-o\\s+(?<objout>\\S*))",
    "|(?(?=-L)",
    "(?<pathfull>(?>-L(?<path>\\S+)))",
    "|(?(?=-[^l])",
    "(?<flagfull>(?>-(?<flag>\\S+)))",
    "|(?<libfull>(?>-\\S(?<lib>\\S+)))))))",
  ]

  def extract_cc_line_parts(cline : String)
    regex_s = MATCH_CC_LINE_PARTS.join
    objcode = ""
    objout = ""
    libpaths = {
      flags:  [] of String,
      values: [] of String,
    }
    libs = {
      flags:  [] of String,
      values: [] of String,
    }
    flags = {
      flags:  [] of String,
      values: [] of String,
    }
    update_part = ->(tp : TPart, up : {String, String}) : TPart {
      flag, value = up
      {
        flags:  flag.empty? ? tp[:flags] : (tp[:flags] + [flag]),
        values: value.empty? ? tp[:values] : (tp[:values] + value.split(':')),
      }
    }
    r = Regex.new(regex_s, Regex::CompileOptions::EXTENDED)
    cline.scan(r) { |m|
      nc = m.named_captures
      objcode = nc["objcode"].to_s unless nc["objcode"].nil?
      objout = nc["objout"].to_s unless nc["objout"].nil?

      libpaths = update_part.call(libpaths, {nc["pathfull"].to_s, nc["path"].to_s})
      libs = update_part.call(libs, {nc["libfull"].to_s, nc["lib"].to_s})
      flags = update_part.call(flags, {nc["flagfull"].to_s, nc["flag"].to_s})
    }
    {objcode: objcode, objout: objout, libpaths: libpaths, libs: libs, flags: flags}
  end

  def get_flags_flags(cc_line_parts)
    cc_line_parts[:flags][:flags]
  end

  def get_libs_flags(cc_line_parts)
    cc_line_parts[:libs][:flags]
  end

  def get_libs_values(cc_line_parts)
    cc_line_parts[:libs][:values]
  end

  def get_libpaths_flags(cc_line_parts)
    cc_line_parts[:libpaths][:flags]
  end

  def get_objcode(cc_line_parts)
    cc_line_parts[:objcode]
  end

  def get_objout(cc_line_parts)
    cc_line_parts[:objout]
  end

  def search_static_libs(path : String, cc_names)
    libs = Dir[
      File.join(path, "**", "*.a"),
    ].sort.map { |l| {File.dirname(l), File.basename(l)} }
    cc_names_libs = cc_names.map { |cc_name| {cc_name, "lib#{cc_name}.a"} }
    cc_names_libs.concat(STDLIBS)
      .reduce([] of {String, String, String}) { |memo, cc_name_lib|
        static_lib = libs.find { |slib| slib[1] == cc_name_lib[1] }
        lib_path = static_lib.nil? ? NOT_FOUND : "-L#{static_lib[0]}"
        memo << {
          lib_path,
          NULL_LIB_NAME == cc_name_lib[0] ? "" : "-l#{cc_name_lib[0]}",
          cc_name_lib[1],
        }
        memo
      }
  end

  def search_cc_libs(path : String)
    libs = Dir[
      File.join(path, "musl*", "**", "*.a"),
    ].sort.map { |l| {File.dirname(l), File.basename(l)} }
  end

  def static_libs_found?(static_libs)
    not_founds = static_libs.select { |slib| slib[0] == NOT_FOUND }
    if not_founds.empty?
      static_libs
    else
      not_founds.each { |slib|
        Utils.error("Lib: #{slib[2]} Flag: #{slib[1]} NOT FOUND")
      }
      exit(NOT_FOUND_CODE)
    end
  end

  def linker_libs(static_libs)
    paths_and_libs = static_libs.reduce({[] of String, [] of String}) { |memo, slib|
      memo[0] << slib[0] unless memo[0].includes?(slib[0])
      memo[1] << slib[1]
      memo
    }
    {
      paths_and_libs[0].sort,
      paths_and_libs[1],
    }
  end

  def execute_crystal(
    projroot : String,
    cryfile : String,
    target = "x86_64-pc-linux-gnu",
    cryflags = [] of String,
    linkflags = [] of String,
    verbose = false
  )
    as_static = cryflags.any?(CRY_STATIC)
    outfile = Utils.prepare_buildroot(projroot, cryfile)
    flags = [
      "--no-color",
      as_static ? "--cross-compile" : nil,
      %(--target "#{target}"),
      linkflags.empty? ? nil : %(--link-flags "#{linkflags.join(" ")}"),
    ].concat(cryflags).compact.uniq
    cmd_line = [
      %(crystal),
      %(build),
      cryfile,
      %(-o),
      outfile,
      flags.join(" "),
    ].join(" ")
    Utils.message("Crystal command line", "\n#{cmd_line}\n") if verbose
    status, out_string, error_string = Utils.execute(cmd_line, projroot)
    if status.exit_code > 0
      Utils.error(error_string, exit_code: status.exit_code)
    else
      if as_static
        Utils.message("Crystal CC generated command line", "\n #{out_string}\n") if verbose
        Driver.extract_cc_line_parts(out_string)
      else
        nil
      end
    end
  end

  def execute_linker(
    objfile : String,
    exefile : String,
    cc = "gcc",
    ld = nil,
    linkflags = [] of String,
    libpaths = [] of String,
    libs = [] of String,
    verbose = false
  )
    flags = [
      "-rdynamic",
      ld.nil? ? nil : %(-fuse-ld=#{ld}),
    ].concat(linkflags).compact.uniq
    cmd_line = [
      cc,
      objfile,
      %(-o),
      exefile,
      flags.join(" "),
      libpaths.join(" "),
      libs.join(" "),
    ].join(" ").strip
    Utils.message("CC command line", "\n #{cmd_line}\n") if verbose
    status, out_string, error_string = Utils.execute(cmd_line)
    if status.exit_code > 0
      Utils.error(error_string, exit_code: status.exit_code)
    end
  end
end
