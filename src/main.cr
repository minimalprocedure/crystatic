# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

require "option_parser"
require "./mod/utils"
require "./mod/version"
require "./mod/crystatic"

include Version

cryfile = ""
exefile = ""
projroot = ""
libsroot = ""
cc = "gcc"
ld = nil
target = "x86_64-pc-linux-gnu"
cryflags = [Driver::CRY_STATIC]
linkflags = [Driver::CC_STATIC]
no_static = false
verbose = false
print_params_and_exit = false

parser = OptionParser.parse do |parser|
  parser.banner = "Crystatic v.: #{Version.get}\nUsage: crystatic source [arguments]"
  parser.on("-o Filename", "--out=Filename", "") { |fn| exefile = fn }
  parser.on("-P Directory", "--project-root=Directory", "") { |fn| projroot = fn }
  parser.on("-L Directory", "--libs-root=Directory", "") { |fn| libsroot = fn }
  parser.on("-C CC", "--cc=CC", "") { |fn| cc = fn }
  parser.on("-D LD", "--ld=LD", "") { |fn| ld = fn }
  parser.on("-T TargetTriple", "--target=TargetTriple", "") { |fn| target = fn }
  parser.on("-F CrystalFlags", "--cry-flags=CrystalFlags", "") { |fn|
    cryflags = cryflags.concat(fn.split(" "))
  }
  parser.on("-K CCLinkerFlags", "--link-flags=CCLinkerFlags", "") { |fn|
    linkflags = linkflags.concat(fn.split(" "))
  }
  parser.on("-d", "--no-static", "") { no_static = true }
  parser.on("-v", "--verbose", "") { verbose = true }
  parser.on("-p", "--print-params", "") {
    verbose = true
    print_params_and_exit = true
  }
  parser.on("-h", "--help", "Show this help") do
    puts parser
    exit
  end
  parser.invalid_option do |flag|
    Utils.error(
      "#{flag}: is not a valid option.\n#{parser}", exit_code: 1)
  end
  parser.missing_option do |flag|
    Utils.error(
      "#{flag}: missing or incomplete option.\n#{parser}", exit_code: 1)
  end
end

cryfile = ARGV[0] if ARGV.size > 0
Utils.error(
  "File: #{cryfile} not exists.\n #{parser}",
  exit_code: 1) unless File.exists?(cryfile)

projroot = FileUtils.pwd if projroot.empty?

exefile = Utils.prepare_buildroot(projroot, cryfile) if exefile.empty?

libsroot = File.join(projroot, "lib") if libsroot.empty?

if no_static
  cryflags.delete(Driver::CRY_STATIC)
  linkflags.delete(Driver::CC_STATIC)
end

if verbose
  Utils.message("Arguments", "")
  Utils.message("source", cryfile)
  Utils.message("output", exefile)
  Utils.message("project-root", projroot)
  Utils.message("libs-root", libsroot)
  Utils.message("cc", cc)
  Utils.message("ld", ld.to_s)
  Utils.message("target", target)
  Utils.message("cry-flags", cryflags)
  Utils.message("link-flags", linkflags)
end
exit if print_params_and_exit

Crystatic.compile(
  cryfile: cryfile,
  exefile: exefile,
  projroot: projroot,
  libsroot: libsroot,
  cc: cc,
  ld: ld,
  target: target,
  cryflags: cryflags,
  linkflags: linkflags,
  verbose: verbose)
