# crystatic

A simple quick 'n dirty static driver for Crystal

## Installation

Download from Alpine repo this apks (rename as zip and unpack) in _static-libs_:  

+ gc-8.2.2
+ gcc
+ gmp-6.2.1
+ icu-73.2
+ libevent-2.1.12
+ libxml2-2.11.4
+ musl-1.2.4
+ pcre2-10.42
+ xz-5.4.3
+ yaml-0.2.5
+ zlib-1.2.13

## Usage

Crystatic v.: 0.5.20230708173055  
Usage: crystatic source [arguments]  
    -o Filename, --out=Filename  
    -P Directory, --project-root=Directory  
    -L Directory, --libs-root=Directory  
    -C CC, --cc=CC  
    -D LD, --ld=LD  
    -T TargetTriple, --target=TargetTriple  
    -F CrystalFlags, --cry-flags=CrystalFlags  
    -K CCLinkerFlags, --link-flags=CCLinkerFlags  
    -v, --verbose  
    -h, --help  Show this help  


